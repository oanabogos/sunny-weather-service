# README #

### Sunny weather forecast microservice ###

This microservice provides weather information and forecast for a given city.
It consumes weather data from World Weather Online API and caches it in-memory so that it shouldn�t be
more than a query per hour per city to the World Weather Online API.

### Configuration ###

* Spring application.properties file

```
weather.provider.hostName=api.worldweatheronline.com
weather.provider.key=YOUR_API_KEY
weather.provider.schema=https
weather.provider.routes=premium/v1/
weather.provider.endpoint=weather.ashx
weather.provider.format=json
weather.provider.days=4 -- to get current day and next 3 days in one query
weather.provider.time=24 -- compress result data on 24 h average
weather.provider.mca=no -- monthly climate avergae 
weather.cache.ttl.sec=3600 -- local cache time to live
```

Note: Configure cache ttl in order to limit number of requests sent to World Weather Online API

* Gradle 4.2 build command

**gradle clean build** or **gradle stage** 

### API specs ###

**GET /current?city={cityName}** - Current weather data for a specific city.

**GET /forecast?city={cityName}** - Next 3 days weather forecast for a specific city.

### High-level architecture ###

![Alt text](/img/high-level-diagram.png)

* Spring Boot microservice with REST API for fetching weather data

* Local Guava Cache with expire after write based on **weather.cache.ttl.sec** value

* Cache loads data from World Weather Online API if there's nothing cached for a specific city

### Internal architecture ###

![Alt text](/img/internal-architecture.png)

**WeatherController**  
REST Spring Controller component exposing GET method to fetch current and forecast weather.

**WeatherService**  
Service component used to delegate requests to data layers, caches, external services and aggregate responses.

**WeatherCache**  
Cache wrapper using in-memory cache structure - Loading Guava Cache as a key-value mapping. 
Values are automatically loaded by the cache and stored until they expire.
Implementations of this interface are expected to be thread-safe. 

**WeatherProvider**  
Client component for weather provider. Sends requests and parses JSON reponses.
Used by loading cache to retreive data.

### Weather data cached structure ###

![Alt text](/img/cached-data-diagram.png)

In order to respect service restriction (number of provider API requests per amount of time), weather data received from the weather
provider needs to be cached.  
Cached record stores both succes or failure requests.  

- Success record stores **WeatherRecord** object with current and forecast data.

- Failed record stores the **WeatherServiceException** thrown by the service.
  
When data is fetched from the cache it can have two possible outcomes:  

- return WeatherRecord

- throw WeatherServiceException




