package ro.fitbit.oana.service.weather.provider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ro.fitbit.oana.service.weather.data.Weather;
import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.CityNotFoundException;
import ro.fitbit.oana.service.weather.exceptions.WeatherProviderAPIException;

import java.net.URI;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link WorldWeatherProvider}
 */
@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class WorldWeatherProviderTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private UriComponentsBuilder weatherURIBuilder;

    private WorldWeatherProvider weatherProvider = null;

    private static final String CITY = "Bucharest";

    private static final String UNKNOWN_CITY = "UnknownCity";

    private static final Weather WEATHER_CURRENT = Weather.builder()
            .tempC(10)
            .tempF(45)
            .humidity(0.5)
            .condition("Sunny")
            .build();

    private static final Weather WEATHER_TOMORROW = Weather.builder()
            .tempC(12)
            .tempF(50)
            .humidity(0.7)
            .condition("Sunny")
            .build();

    private static final Weather WEATHER_AFTER_TOMORROW = Weather.builder()
            .tempC(11)
            .tempF(49)
            .humidity(0.8)
            .condition("Rainy")
            .build();

    private static final Weather WEATHER_AFTER_AFTER_TOMORROW = Weather.builder()
            .tempC(13)
            .tempF(52)
            .humidity(0.3)
            .condition("Rainy")
            .build();

    private static final WeatherRecord WEATHER_RECORD = new WeatherRecord(WEATHER_CURRENT, Arrays.asList(WEATHER_TOMORROW, WEATHER_AFTER_TOMORROW, WEATHER_AFTER_AFTER_TOMORROW));

    private static String SUCCESS_JSON = "{\"data\":{\"request\":[{\"type\":\"City\",\"query\":\"Bucharest, Romania\"}],\"current_condition\":[{\"observation_time\":\"07:57 PM\",\"temp_C\":\"10\",\"temp_F\":\"45\",\"weatherCode\":\"143\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png\"}],\"weatherDesc\":[{\"value\":\"Sunny\"}],\"windspeedMiles\":\"4\",\"windspeedKmph\":\"6\",\"winddirDegree\":\"40\",\"winddir16Point\":\"NE\",\"precipMM\":\"0.0\",\"humidity\":\"50\",\"visibility\":\"3\",\"pressure\":\"1013\",\"cloudcover\":\"25\",\"FeelsLikeC\":\"30\",\"FeelsLikeF\":\"87\"}],\"weather\":[{\"date\":\"2017-10-30\",\"astronomy\":[{\"sunrise\":\"06:38 AM\",\"sunset\":\"06:06 PM\",\"moonrise\":\"02:48 PM\",\"moonset\":\"01:49 AM\"}],\"maxtempC\":\"34\",\"maxtempF\":\"94\",\"mintempC\":\"29\",\"mintempF\":\"84\",\"totalSnow_cm\":\"0.0\",\"sunHour\":\"0.0\",\"uvIndex\":\"8\",\"hourly\":[{\"time\":\"24\",\"tempC\":\"34\",\"tempF\":\"94\",\"windspeedMiles\":\"7\",\"windspeedKmph\":\"11\",\"winddirDegree\":\"127\",\"winddir16Point\":\"SE\",\"weatherCode\":\"113\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png\"}],\"weatherDesc\":[{\"value\":\"Sunny\"}],\"precipMM\":\"0.0\",\"humidity\":\"48\",\"visibility\":\"20\",\"pressure\":\"1014\",\"cloudcover\":\"0\",\"HeatIndexC\":\"33\",\"HeatIndexF\":\"92\",\"DewPointC\":\"19\",\"DewPointF\":\"66\",\"WindChillC\":\"31\",\"WindChillF\":\"88\",\"WindGustMiles\":\"9\",\"WindGustKmph\":\"14\",\"FeelsLikeC\":\"33\",\"FeelsLikeF\":\"92\",\"chanceofrain\":\"0\",\"chanceofremdry\":\"0\",\"chanceofwindy\":\"0\",\"chanceofovercast\":\"0\",\"chanceofsunshine\":\"90\",\"chanceoffrost\":\"0\",\"chanceofhightemp\":\"96\",\"chanceoffog\":\"0\",\"chanceofsnow\":\"0\",\"chanceofthunder\":\"0\"}]},{\"date\":\"2017-10-31\",\"astronomy\":[{\"sunrise\":\"06:38 AM\",\"sunset\":\"06:06 PM\",\"moonrise\":\"03:30 PM\",\"moonset\":\"02:42 AM\"}],\"maxtempC\":\"35\",\"maxtempF\":\"94\",\"mintempC\":\"25\",\"mintempF\":\"78\",\"totalSnow_cm\":\"0.0\",\"sunHour\":\"0.0\",\"uvIndex\":\"8\",\"hourly\":[{\"time\":\"24\",\"tempC\":\"12\",\"tempF\":\"50\",\"windspeedMiles\":\"7\",\"windspeedKmph\":\"12\",\"winddirDegree\":\"129\",\"winddir16Point\":\"SE\",\"weatherCode\":\"113\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png\"}],\"weatherDesc\":[{\"value\":\"Sunny\"}],\"precipMM\":\"0.0\",\"humidity\":\"70\",\"visibility\":\"20\",\"pressure\":\"1012\",\"cloudcover\":\"0\",\"HeatIndexC\":\"33\",\"HeatIndexF\":\"92\",\"DewPointC\":\"19\",\"DewPointF\":\"66\",\"WindChillC\":\"31\",\"WindChillF\":\"87\",\"WindGustMiles\":\"9\",\"WindGustKmph\":\"14\",\"FeelsLikeC\":\"33\",\"FeelsLikeF\":\"92\",\"chanceofrain\":\"0\",\"chanceofremdry\":\"0\",\"chanceofwindy\":\"0\",\"chanceofovercast\":\"0\",\"chanceofsunshine\":\"90\",\"chanceoffrost\":\"0\",\"chanceofhightemp\":\"92\",\"chanceoffog\":\"0\",\"chanceofsnow\":\"0\",\"chanceofthunder\":\"0\"}]},{\"date\":\"2017-11-01\",\"astronomy\":[{\"sunrise\":\"06:39 AM\",\"sunset\":\"06:05 PM\",\"moonrise\":\"04:12 PM\",\"moonset\":\"03:37 AM\"}],\"maxtempC\":\"35\",\"maxtempF\":\"94\",\"mintempC\":\"26\",\"mintempF\":\"79\",\"totalSnow_cm\":\"0.0\",\"sunHour\":\"0.0\",\"uvIndex\":\"9\",\"hourly\":[{\"time\":\"24\",\"tempC\":\"11\",\"tempF\":\"49\",\"windspeedMiles\":\"8\",\"windspeedKmph\":\"13\",\"winddirDegree\":\"132\",\"winddir16Point\":\"SE\",\"weatherCode\":\"113\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png\"}],\"weatherDesc\":[{\"value\":\"Rainy\"}],\"precipMM\":\"0.0\",\"humidity\":\"80\",\"visibility\":\"20\",\"pressure\":\"1012\",\"cloudcover\":\"0\",\"HeatIndexC\":\"33\",\"HeatIndexF\":\"91\",\"DewPointC\":\"19\",\"DewPointF\":\"65\",\"WindChillC\":\"31\",\"WindChillF\":\"87\",\"WindGustMiles\":\"9\",\"WindGustKmph\":\"15\",\"FeelsLikeC\":\"33\",\"FeelsLikeF\":\"91\",\"chanceofrain\":\"0\",\"chanceofremdry\":\"0\",\"chanceofwindy\":\"0\",\"chanceofovercast\":\"0\",\"chanceofsunshine\":\"90\",\"chanceoffrost\":\"0\",\"chanceofhightemp\":\"88\",\"chanceoffog\":\"0\",\"chanceofsnow\":\"0\",\"chanceofthunder\":\"0\"}]},{\"date\":\"2017-11-02\",\"astronomy\":[{\"sunrise\":\"06:39 AM\",\"sunset\":\"06:05 PM\",\"moonrise\":\"04:55 PM\",\"moonset\":\"04:33 AM\"}],\"maxtempC\":\"33\",\"maxtempF\":\"92\",\"mintempC\":\"28\",\"mintempF\":\"82\",\"totalSnow_cm\":\"0.0\",\"sunHour\":\"0.0\",\"uvIndex\":\"9\",\"hourly\":[{\"time\":\"24\",\"tempC\":\"13\",\"tempF\":\"52\",\"windspeedMiles\":\"7\",\"windspeedKmph\":\"12\",\"winddirDegree\":\"131\",\"winddir16Point\":\"SE\",\"weatherCode\":\"113\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png\"}],\"weatherDesc\":[{\"value\":\"Rainy\"}],\"precipMM\":\"0.0\",\"humidity\":\"30\",\"visibility\":\"20\",\"pressure\":\"1012\",\"cloudcover\":\"0\",\"HeatIndexC\":\"33\",\"HeatIndexF\":\"91\",\"DewPointC\":\"18\",\"DewPointF\":\"65\",\"WindChillC\":\"31\",\"WindChillF\":\"88\",\"WindGustMiles\":\"9\",\"WindGustKmph\":\"14\",\"FeelsLikeC\":\"33\",\"FeelsLikeF\":\"91\",\"chanceofrain\":\"0\",\"chanceofremdry\":\"0\",\"chanceofwindy\":\"0\",\"chanceofovercast\":\"0\",\"chanceofsunshine\":\"90\",\"chanceoffrost\":\"0\",\"chanceofhightemp\":\"84\",\"chanceoffog\":\"0\",\"chanceofsnow\":\"0\",\"chanceofthunder\":\"0\"}]}]}}";

    private static String ERROR_JSON = "{\"data\":{\"error\":[{\"msg\":\"Unable to find any matching weather location to the query submitted!\"}]}}";

    @Before
    public void setUp() {
        weatherProvider = new WorldWeatherProvider(restTemplate, weatherURIBuilder);
        when(weatherURIBuilder.replaceQueryParam(any(), any())).thenReturn(weatherURIBuilder);
        when(weatherURIBuilder.build()).thenReturn(UriComponentsBuilder.newInstance().build());
    }

    @Test
    public void getWeatherData_successFromWeatherAPI_returnWeatherRecord() throws Exception {
        //given
        ResponseEntity responseEntity = new ResponseEntity(SUCCESS_JSON, HttpStatus.OK);
        when(restTemplate.getForEntity(any(URI.class), any())).thenReturn(responseEntity);

        //when
        WeatherRecord result = weatherProvider.getWeatherData(CITY);

        //then
        assertNotNull(result);
        assertEquals(WEATHER_RECORD.getCurrent(), result.getCurrent());
        assertEquals(WEATHER_RECORD.getForecast(), result.getForecast());
    }

    @Test(expected = CityNotFoundException.class)
    public void getWeatherData_errorFromWeatherAPI_throwCityNotFoundException() throws Exception {
        //given
        ResponseEntity responseEntity = new ResponseEntity(ERROR_JSON, HttpStatus.OK);
        when(restTemplate.getForEntity(any(URI.class), any())).thenReturn(responseEntity);

        //when
        weatherProvider.getWeatherData(UNKNOWN_CITY);
    }

    @Test(expected = WeatherProviderAPIException.class)
    public void getWeatherData_notOkStatusFromWeatherAPI_throwWeatherProviderAPIException() throws Exception {
        //given
        ResponseEntity responseEntity = new ResponseEntity(ERROR_JSON, HttpStatus.INTERNAL_SERVER_ERROR);
        when(restTemplate.getForEntity(any(URI.class), any())).thenReturn(responseEntity);

        //when
        weatherProvider.getWeatherData(CITY);
    }
}
