package ro.fitbit.oana.service.weather.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ro.fitbit.oana.service.weather.cache.WeatherCache;
import ro.fitbit.oana.service.weather.data.Weather;
import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.CityNotFoundException;
import ro.fitbit.oana.service.weather.exceptions.CityNotProvidedException;
import ro.fitbit.oana.service.weather.exceptions.WeatherProviderAPIException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link WeatherService}
 */
@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceTest {

    @Mock
    private WeatherCache weatherCache;

    private WeatherService weatherService;

    private static final String CITY = "Bucharest";

    private static final String UNKNOWN_CITY = "UnknownCity";

    private static final String EMPTY_CITY = "";

    private static final Weather WEATHER_CURRENT = Weather.builder()
            .tempC(10)
            .tempF(45)
            .humidity(0.5)
            .condition("Sunny")
            .build();

    private static final Weather WEATHER_TOMORROW = Weather.builder()
            .tempC(12)
            .tempF(50)
            .humidity(0.7)
            .condition("Sunny")
            .build();

    private static final Weather WEATHER_AFTER_TOMORROW = Weather.builder()
            .tempC(11)
            .tempF(49)
            .humidity(1)
            .condition("Rainy")
            .build();

    private static final Weather WEATHER_AFTER_AFTER_TOMORROW = Weather.builder()
            .tempC(13)
            .tempF(52)
            .humidity(15)
            .condition("Rainy")
            .build();

    private static final WeatherRecord WEATHER_RECORD = new WeatherRecord(WEATHER_CURRENT, Arrays.asList(WEATHER_TOMORROW, WEATHER_AFTER_TOMORROW, WEATHER_AFTER_AFTER_TOMORROW));

    @Before
    public void setUp() {
        weatherService = new WeatherService(weatherCache);
    }

    @Test
    public void currentWeather_withCorrectCity_returnsCurrentWeather() throws Exception {
        //given
        when(weatherCache.get(CITY)).thenReturn(WEATHER_RECORD);

        //when
        Optional<Weather> result = weatherService.currentWeather(CITY);

        //then
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(WEATHER_CURRENT, result.get());
        verify(weatherCache, times(1)).get(CITY);
    }

    @Test
    public void currentWeather_withEmptyCity_throwsCityNotProvidedException() throws Exception {
        //when
        try {
            weatherService.currentWeather(EMPTY_CITY);
            fail("Expected CityNotProvidedException");
        } catch (CityNotProvidedException e) {}

        //then
        verify(weatherCache, times(0)).get(EMPTY_CITY);
    }

    @Test
    public void currentWeather_withUnknownCity_throwsCityNotFoundException() throws Exception {
        //given
        when(weatherCache.get(UNKNOWN_CITY)).thenThrow(CityNotFoundException.class);

        //when
        try {
            weatherService.currentWeather(UNKNOWN_CITY);
            fail("Expected CityNotFoundException");
        } catch (CityNotFoundException e) {}

        //then
        verify(weatherCache, times(1)).get(UNKNOWN_CITY);
    }

    @Test
    public void currentWeather_withFailedAPIRequest_throwsWeatherProviderAPIException() throws Exception {
        //given
        when(weatherCache.get(CITY)).thenThrow(WeatherProviderAPIException.class);

        //when
        try {
            weatherService.currentWeather(CITY);
            fail("Expected WeatherProviderAPIException");
        } catch (WeatherProviderAPIException e) {}

        //then
        verify(weatherCache, times(1)).get(CITY);
    }

    @Test
    public void forecastWeather_withCorrectCity_returnsForecastWeather() throws Exception {
        //given
        when(weatherCache.get(CITY)).thenReturn(WEATHER_RECORD);

        //when
        List<Weather> result = weatherService.forecastWeather(CITY);

        //then
        assertNotNull(result);
        assertTrue(!result.isEmpty());
        assertEquals(Arrays.asList(WEATHER_TOMORROW, WEATHER_AFTER_TOMORROW, WEATHER_AFTER_AFTER_TOMORROW), result);
        verify(weatherCache, times(1)).get(CITY);
    }

    @Test
    public void forecastWeather_withEmptyCity_throwsCityNotProvidedException() throws Exception {
        //when
        try {
            weatherService.forecastWeather(EMPTY_CITY);
            fail("Expected CityNotProvidedException");
        } catch (CityNotProvidedException e) {}

        //then
        verify(weatherCache, times(0)).get(EMPTY_CITY);
    }

    @Test
    public void forecastWeather_withUnknownCity_throwsCityNotFoundException() throws Exception {
        //given
        when(weatherCache.get(UNKNOWN_CITY)).thenThrow(CityNotFoundException.class);

        //when
        try {
            weatherService.forecastWeather(UNKNOWN_CITY);
            fail("Expected CityNotFoundException");
        } catch (CityNotFoundException e) {}

        //then
        verify(weatherCache, times(1)).get(UNKNOWN_CITY);
    }

    @Test
    public void forecastWeather_withFailedAPIRequest_throwsWeatherProviderAPIException() throws Exception {
        //given
        when(weatherCache.get(CITY)).thenThrow(WeatherProviderAPIException.class);

        //when
        try {
            weatherService.forecastWeather(CITY);
            fail("Expected WeatherProviderAPIException");
        } catch (WeatherProviderAPIException e) {}

        //then
        verify(weatherCache, times(1)).get(CITY);
    }
}
