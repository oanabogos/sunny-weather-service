package ro.fitbit.oana.service.weather.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ro.fitbit.oana.service.weather.data.Weather;
import ro.fitbit.oana.service.weather.exceptions.CityNotFoundException;
import ro.fitbit.oana.service.weather.exceptions.CityNotProvidedException;
import ro.fitbit.oana.service.weather.exceptions.WeatherProviderAPIException;
import ro.fitbit.oana.service.weather.service.WeatherService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link WeatherController}
 */
@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class WeatherControllerTest {

    @Mock
    private WeatherService weatherService;

    private WeatherController weatherController;

    private static final String CITY = "Bucharest";

    private static final String UNKNOWN_CITY = "UnknownCity";

    private static final String EMPTY_CITY = "";

    private static final int TEMPC = 10;

    private static final int TEMPF = 45;

    private static final double HUMIDITY = 0.5;

    private static final String CONDITION = "Sunny";

    private static final Weather WEATHER_CURRENT = Weather.builder()
            .tempC(TEMPC)
            .tempF(TEMPF)
            .humidity(HUMIDITY)
            .condition(CONDITION)
            .build();

    @Before
    public void setUp() {
        weatherController = new WeatherController(weatherService);
    }

    @Test
    public void getCurrent_whenCorrectCity_returnsWeatherAnd200OK() throws Exception {
        //given
        when(weatherService.currentWeather(CITY)).thenReturn(Optional.of(WEATHER_CURRENT));

        //when
        ResponseEntity<Weather> response = weatherController.getCurrent(CITY);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(WEATHER_CURRENT, response.getBody());
        verify(weatherService, times(1)).currentWeather(CITY);
    }

    @Test
    public void getCurrent_whenUnknownCity_throwCityNotFoundException() throws Exception {
        //given
        when(weatherService.currentWeather(UNKNOWN_CITY)).thenThrow(CityNotFoundException.class);

        //when
        try {
            weatherController.getCurrent(UNKNOWN_CITY);
            fail("Expected CityNotFoundException");
        } catch (CityNotFoundException e) {}

        //then
        verify(weatherService, times(1)).currentWeather(UNKNOWN_CITY);
    }

    @Test
    public void getCurrent_whenEmptyCity_throwCityNotProvidedException() throws Exception {
        //given
        when(weatherService.currentWeather(EMPTY_CITY)).thenThrow(CityNotProvidedException.class);

        //when
        try {
            weatherController.getCurrent(EMPTY_CITY);
            fail("Expected CityNotProvidedException");
        } catch (CityNotProvidedException e) {}

        //then
        verify(weatherService, times(1)).currentWeather(EMPTY_CITY);
    }

    @Test
    public void getCurrent_whenCannotGetCity_throwWeatherProviderAPIException() throws Exception {
        //given
        when(weatherService.currentWeather(CITY)).thenThrow(WeatherProviderAPIException.class);

        //when
        try {
            weatherController.getCurrent(CITY);
            fail("Expected WeatherProviderAPIException");
        } catch (WeatherProviderAPIException e){}

        //then
        verify(weatherService, times(1)).currentWeather(CITY);

    }

    @Test
    public void getForecast_whenCorrectCity_returnsWeatherListAnd200OK() throws Exception {
        //given
        when(weatherService.forecastWeather(CITY)).thenReturn(Arrays.asList(WEATHER_CURRENT));

        //when
        ResponseEntity<List<Weather>> response = weatherController.getForecast(CITY);

        //then
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(Arrays.asList(WEATHER_CURRENT), response.getBody());
        verify(weatherService, times(1)).forecastWeather(CITY);
    }

    @Test
    public void getForecast_whenUnknownCity_throwCityNotFoundException() throws Exception {
        //given
        when(weatherService.forecastWeather(UNKNOWN_CITY)).thenThrow(CityNotFoundException.class);

        //when
        try {
            weatherController.getForecast(UNKNOWN_CITY);
            fail("Expected CityNotFoundException");
        } catch (CityNotFoundException e) {}

        //then
        verify(weatherService, times(1)).forecastWeather(UNKNOWN_CITY);
    }

    @Test
    public void getForecast_whenEmptyCity_throwCityNotProvidedException() throws Exception {
        //given
        when(weatherService.forecastWeather(EMPTY_CITY)).thenThrow(CityNotProvidedException.class);

        //when
        try {
            weatherController.getForecast(EMPTY_CITY);
            fail("Expected CityNotProvidedException");
        } catch (CityNotProvidedException e) {}

        //then
        verify(weatherService, times(1)).forecastWeather(EMPTY_CITY);
    }

    @Test
    public void getForecast_whenCannotGetCity_throwWeatherProviderAPIException() throws Exception {
        //given
        when(weatherService.forecastWeather(CITY)).thenThrow(WeatherProviderAPIException.class);

        //when
        try {
            weatherController.getForecast(CITY);
            fail("Expected WeatherProviderAPIException");
        } catch (WeatherProviderAPIException e) {}

        //then
        verify(weatherService, times(1)).forecastWeather(CITY);
    }
}