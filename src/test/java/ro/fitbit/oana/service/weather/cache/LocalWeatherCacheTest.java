package ro.fitbit.oana.service.weather.cache;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ro.fitbit.oana.service.weather.data.Weather;
import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.CityNotFoundException;
import ro.fitbit.oana.service.weather.exceptions.WeatherProviderAPIException;
import ro.fitbit.oana.service.weather.provider.WeatherProvider;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

/**
 * Unit tests for {@link LocalWeatherCacheTest}
 */
@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class LocalWeatherCacheTest {

    @Mock
    private WeatherProvider weatherProvider;

    private LocalWeatherCache weatherCache;

    private static final long CACHE_TTL = 1;

    private static final String CITY = "Bucharest";

    private static final int TEMPC = 10;

    private static final int TEMPF = 45;

    private static final double HUMIDITY = 0.5;

    private static final String CONDITION = "Sunny";

    private static final Weather WEATHER_CURRENT = Weather.builder()
            .tempC(TEMPC)
            .tempF(TEMPF)
            .humidity(HUMIDITY)
            .condition(CONDITION)
            .build();

    private static final Weather WEATHER_TOMORROW = Weather.builder()
            .tempC(TEMPC)
            .tempF(TEMPF)
            .humidity(HUMIDITY)
            .condition(CONDITION)
            .build();

    private static final Weather WEATHER_AFTER_TOMORROW = Weather.builder()
            .tempC(TEMPC)
            .tempF(TEMPF)
            .humidity(HUMIDITY)
            .condition(CONDITION)
            .build();

    private static final Weather WEATHER_AFTER_AFTER_TOMORROW = Weather.builder()
            .tempC(TEMPC)
            .tempF(TEMPF)
            .humidity(HUMIDITY)
            .condition(CONDITION)
            .build();

    private static final WeatherRecord WEATHER_RECORD = new WeatherRecord(WEATHER_CURRENT, Arrays.asList(WEATHER_TOMORROW, WEATHER_AFTER_TOMORROW, WEATHER_AFTER_AFTER_TOMORROW));

    @Before
    public void setUp() {
        weatherCache = new LocalWeatherCache(weatherProvider, CACHE_TTL);
    }

    @Test
    public void get_successFromWeatherProvider_returnWeatherRecord() throws Exception {
        //given
        when(weatherProvider.getWeatherData(CITY)).thenReturn(WEATHER_RECORD);

        //when
        WeatherRecord weatherRecord = weatherCache.get(CITY);

        //then
        assertNotNull(weatherRecord);
        assertEquals(WEATHER_RECORD, weatherRecord);
        verify(weatherProvider, times(1)).getWeatherData(CITY);

        //when fetch again
        weatherRecord = weatherCache.get(CITY);

        //then
        assertNotNull(weatherRecord);
        assertEquals(WEATHER_RECORD, weatherRecord);
        //should not hit weather provider anymore
        verify(weatherProvider, times(1)).getWeatherData(CITY);
    }

    @Test
    public void get_successFromWeatherProvider_expireCache_returnWeatherRecord() throws Exception {
        //given
        when(weatherProvider.getWeatherData(CITY)).thenReturn(WEATHER_RECORD);

        //when
        WeatherRecord weatherRecord = weatherCache.get(CITY);

        //then
        assertNotNull(weatherRecord);
        assertEquals(WEATHER_RECORD, weatherRecord);
        verify(weatherProvider, times(1)).getWeatherData(CITY);

        //Sleep 1 sec to get expired cache
        Thread.sleep(1000);

        //when fetch again
        weatherRecord = weatherCache.get(CITY);

        //then
        assertNotNull(weatherRecord);
        assertEquals(WEATHER_RECORD, weatherRecord);
        //should not hit weather provider anymore
        verify(weatherProvider, times(2)).getWeatherData(CITY);
    }

    @Test
    public void get_failureFromWeatherProvider_throwCityNotFoundException() throws Exception {
        //given
        when(weatherProvider.getWeatherData(CITY)).thenThrow(CityNotFoundException.class);

        //when
        try {
            weatherCache.get(CITY);
            fail("Expected CityNotFoundException");
        } catch (CityNotFoundException e) {}

        //then
        verify(weatherProvider, times(1)).getWeatherData(CITY);

        //when get again
        try {
            weatherCache.get(CITY);
            fail("Expected CityNotFoundException");
        } catch (CityNotFoundException e) {}

        //then
        verify(weatherProvider, times(1)).getWeatherData(CITY);
    }

    @Test
    public void get_failureFromWeatherProvider_throwWeatherProviderAPIException() throws Exception {
        //given
        when(weatherProvider.getWeatherData(CITY)).thenThrow(WeatherProviderAPIException.class);

        //when
        try {
            weatherCache.get(CITY);
            fail("Expected WeatherProviderAPIException");
        } catch (WeatherProviderAPIException e) {}

        //then
        verify(weatherProvider, times(1)).getWeatherData(CITY);

        //when get again
        try {
            weatherCache.get(CITY);
            fail("Expected WeatherProviderAPIException");
        } catch (WeatherProviderAPIException e) {}

        //then
        verify(weatherProvider, times(1)).getWeatherData(CITY);
    }

}
