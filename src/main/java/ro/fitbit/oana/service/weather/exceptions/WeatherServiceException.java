package ro.fitbit.oana.service.weather.exceptions;

/**
 * Base class for custom weather service exceptions.
 */
public class WeatherServiceException extends Exception {

    public WeatherServiceException(String message) {
        super(message);
    }

}
