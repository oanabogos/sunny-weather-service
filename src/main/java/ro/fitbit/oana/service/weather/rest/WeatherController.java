package ro.fitbit.oana.service.weather.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.fitbit.oana.service.weather.data.Weather;
import ro.fitbit.oana.service.weather.exceptions.WeatherServiceException;
import ro.fitbit.oana.service.weather.service.WeatherService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@RestController
public class WeatherController {

    final WeatherService weatherService;

    @Value("${welcome.message}")
    private String welcomeMessage;

    @Autowired
    public WeatherController(WeatherService weatherService) {
        this.weatherService = Objects.requireNonNull(weatherService, "weatherService");
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Weather> get() {
        return new ResponseEntity(welcomeMessage, HttpStatus.OK);
    }

    /**
     * GET current weather data for a specific city
     *
     * @return a response entity which includes the Weather data and a corresponding HTTP status code for the success/failure of the request
     */
    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public ResponseEntity<Weather> getCurrent(@RequestParam(value = "city") String city) throws Exception {
        log.info("Received request /current?city={}", city);
        Weather weatherResponse = null;
        try {
            Optional<Weather> weather = weatherService.currentWeather(city.trim());
            if (weather.isPresent()) {
                weatherResponse = weather.get();
            }
        } catch (WeatherServiceException e) {
            log.info(e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return new ResponseEntity<>(weatherResponse, HttpStatus.OK);
    }

    /**
     * GET weather forecast for the next 3 days for a specific city
     *
     * @return a response entity which includes the Weather data and a corresponding HTTP status code for the success/failure of the request
     */
    @RequestMapping(value = "/forecast", method = RequestMethod.GET)
    public ResponseEntity<List<Weather>> getForecast(@RequestParam("city") String city) throws Exception {
        log.info("Received request /forecast?city={}", city);
        List<Weather> weatherData;
        try {
            weatherData = weatherService.forecastWeather(city.trim());
        } catch (WeatherServiceException e) {
            log.info(e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return new ResponseEntity<>(weatherData, HttpStatus.OK);
    }
}