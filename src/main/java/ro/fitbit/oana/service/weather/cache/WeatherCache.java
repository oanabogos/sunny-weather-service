package ro.fitbit.oana.service.weather.cache;

import ro.fitbit.oana.service.weather.data.WeatherRecord;

/**
 * In memory weather cache behavior.
 */
public interface WeatherCache {

    /**
     * Get data from cache by query.
     *
     * @param query The key query param, unique identifier in cache.
     * @return weather data.
     */
    WeatherRecord get(String query) throws Exception;
}
