package ro.fitbit.oana.service.weather.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Weather DTO.
 */
@Builder
@AllArgsConstructor
@JsonSerialize
@EqualsAndHashCode
@Getter
public class Weather {

    @JsonProperty
    private final int tempC;

    @JsonProperty
    private final int tempF;

    @JsonProperty
    private final String condition;

    @JsonProperty
    private final double humidity;
}
