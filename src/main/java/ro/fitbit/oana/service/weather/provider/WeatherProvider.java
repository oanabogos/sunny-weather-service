package ro.fitbit.oana.service.weather.provider;

import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.CityNotFoundException;
import ro.fitbit.oana.service.weather.exceptions.WeatherProviderAPIException;

/**
 * Describes client behavior of a Weather provider external service.
 */
public interface WeatherProvider {

    /**
     * Fetch Weather data from Weather provider for a specific city.
     *
     * @param city queried city.
     * @return weather object.
     */
    WeatherRecord getWeatherData(String city) throws CityNotFoundException, WeatherProviderAPIException;
}
