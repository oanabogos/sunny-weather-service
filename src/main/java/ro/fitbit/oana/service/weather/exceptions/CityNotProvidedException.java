package ro.fitbit.oana.service.weather.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception thrown when the city name is not provided in requests.
 * Mapped to HTTP 400 BAD REQUEST response.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Please provide a city name")
public class CityNotProvidedException extends WeatherServiceException {

    public CityNotProvidedException(String message) {
        super(message);
    }

}
