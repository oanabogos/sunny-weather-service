package ro.fitbit.oana.service.weather.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.WeatherServiceException;
import ro.fitbit.oana.service.weather.provider.WeatherProvider;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Local caching implementation using Guava Loading Cache.
 */
@Slf4j
@Component
public class LocalWeatherCache implements WeatherCache {

    private LoadingCache<String, CachedRecord> cache;

    private final WeatherProvider weatherProvider;

    @Autowired
    public LocalWeatherCache(WeatherProvider weatherProvider,
                             @Value("${weather.cache.ttl.sec}") long cacheTtl) {
        this.weatherProvider = Objects.requireNonNull(weatherProvider, "weatherProvider");
        buildCache(cacheTtl);
    }

    @Override
    public WeatherRecord get(String city) throws Exception {
        return cache.get(city).get();
    }

    /**
     * Initialize Guava cache with time to live and loading method that delegates to weather provider.
     */
    private void buildCache(long cacheTtl) {
        this.cache = CacheBuilder.newBuilder()
                .expireAfterWrite(cacheTtl, TimeUnit.SECONDS)
                .build(new CacheLoader<String, CachedRecord>() {
                    @Override
                    public CachedRecord load(String city) throws Exception {
                        try {
                            return CachedRecord.success(weatherProvider.getWeatherData(city));
                        } catch (WeatherServiceException e) {
                            return CachedRecord.failure(e);
                        }
                    }
                });
    }
}
