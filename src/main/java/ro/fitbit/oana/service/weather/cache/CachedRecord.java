package ro.fitbit.oana.service.weather.cache;

import lombok.AllArgsConstructor;
import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.WeatherServiceException;

/**
 * Cache record wrapper with weather data or failed weather requests.
 * Used to cache both failed and success responses.
 */
public abstract class CachedRecord {

    private CachedRecord() {
        // No instances outside this class
    }

    public static CachedRecord success(WeatherRecord record) {
        return new Success(record);
    }

    public static CachedRecord failure(WeatherServiceException exception) {
        return new Failure(exception);
    }

    public abstract WeatherRecord get() throws WeatherServiceException;

    /**
     * Holds the weather record as a success cache record.
     */
    @AllArgsConstructor
    private static final class Success extends CachedRecord {

        final WeatherRecord record;

        @Override
        public final WeatherRecord get() throws WeatherServiceException {
            return record;
        }
    }

    /**
     * Holds the WeatherServiceException as a failed cache record.
     */
    @AllArgsConstructor
    private static final class Failure extends CachedRecord {

        final WeatherServiceException exception;

        @Override
        public final WeatherRecord get() throws WeatherServiceException {
            throw exception;
        }
    }

}