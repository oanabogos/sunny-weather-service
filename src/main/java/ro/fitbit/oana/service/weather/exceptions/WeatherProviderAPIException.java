package ro.fitbit.oana.service.weather.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception thrown when the Weather Provider API doesn't return data.
 * Mapped to HTTP 500 INTERNAL SERVER ERROR response.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Could not fetch data from Weather Provider API")
public class WeatherProviderAPIException extends WeatherServiceException {

    public WeatherProviderAPIException(String message) {
        super(message);
    }

}
