package ro.fitbit.oana.service.weather.data;

import com.google.common.collect.ImmutableList;
import lombok.Getter;

import java.util.List;

/**
 * Cached weather record.
 * Stores the current day weather and the list of weather forecasts for the next 3 days.
 */
public class WeatherRecord {

    @Getter
    private final Weather current;

    @Getter
    private final List<Weather> forecast;

    public WeatherRecord(Weather current, List<Weather> forecast) {
        this.current = current;
        this.forecast = ImmutableList.copyOf(forecast);
    }

}