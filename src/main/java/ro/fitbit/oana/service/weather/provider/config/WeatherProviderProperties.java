package ro.fitbit.oana.service.weather.provider.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "weather.provider")
public class WeatherProviderProperties {

    @Getter
    @Setter
    private String hostName;

    @Getter
    @Setter
    private String schema;

    @Getter
    @Setter
    private String routes;

    @Getter
    @Setter
    private String endpoint;

    @Getter
    @Setter
    private String key;

    @Getter
    @Setter
    private String format;

    @Getter
    @Setter
    private String days;

    @Getter
    @Setter
    private String time;

    @Getter
    @Setter
    private String mca;
}
