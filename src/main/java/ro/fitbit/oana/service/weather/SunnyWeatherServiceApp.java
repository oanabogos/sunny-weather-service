package ro.fitbit.oana.service.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SunnyWeatherServiceApp {

    public static void main(String[] args) {
        SpringApplication.run(SunnyWeatherServiceApp.class);
    }
}
