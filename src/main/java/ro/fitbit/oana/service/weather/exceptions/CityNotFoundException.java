package ro.fitbit.oana.service.weather.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception thrown when the city name is not provided in requests.
 * Mapped to HTTP 404 NOT FOUND response.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Unable to find any matching weather location to the city submitted")
public class CityNotFoundException extends WeatherServiceException {

    public CityNotFoundException(String message) {
        super(message);
    }

}
