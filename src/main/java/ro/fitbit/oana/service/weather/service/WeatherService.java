package ro.fitbit.oana.service.weather.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ro.fitbit.oana.service.weather.cache.WeatherCache;
import ro.fitbit.oana.service.weather.data.Weather;
import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.CityNotProvidedException;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Weather service logic.
 */
@Slf4j
@Service
public class WeatherService {

    final WeatherCache weatherCache;

    @Autowired
    public WeatherService(WeatherCache weatherCache) {
        this.weatherCache = Objects.requireNonNull(weatherCache, "weatherCache");
    }

    /**
     * Get current weather for a specific city.
     *
     * @param city City queried for weather.
     * @return Current weather forecast.
     * @throws Exception if something wrong happens during current weather retrieval.
     */
    public Optional<Weather> currentWeather(String city) throws Exception {
        WeatherRecord weatherRecord = buildWeatherData(city);
        return weatherRecord != null ? Optional.of(weatherRecord.getCurrent()) : Optional.empty();
    }

    /**
     * Get forecast weather for the next 3 days for a specific city.
     *
     * @param city City queried for weather.
     * @return List of weather forecasts.
     * @throws Exception if something wrong happens during forecast weather retrieval.
     */
    public List<Weather> forecastWeather(String city) throws Exception {
        WeatherRecord weatherRecord = buildWeatherData(city);
        return weatherRecord != null ? weatherRecord.getForecast() : Collections.emptyList();
    }

    /**
     * Fetch weather data from cache.
     */
    private WeatherRecord buildWeatherData(String city) throws Exception {
        if (StringUtils.isEmpty(city)) {
            throw new CityNotProvidedException("Please provide a city name");
        }
        return weatherCache.get(city);
    }

}
