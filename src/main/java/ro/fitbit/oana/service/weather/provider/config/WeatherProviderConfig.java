package ro.fitbit.oana.service.weather.provider.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class WeatherProviderConfig {

    private static final String KEY_QUERY_PARAM = "key";

    private static final String FORMAT_QUERY_PARAM = "format";

    private static final String DAYS_QUERY_PARAM = "num_of_days";

    private static final String TP_QUERY_PARAM = "tp";

    private static final String MCA_QUERY_PARAM = "mca";

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public WeatherProviderProperties weatherProviderProperties() {
        return new WeatherProviderProperties();
    }

    @Bean
    public UriComponentsBuilder weatherURIBuilder() {
        WeatherProviderProperties properties = weatherProviderProperties();
        return UriComponentsBuilder
                .newInstance()
                .scheme(properties.getSchema())
                .host(properties.getHostName())
                .path(properties.getRoutes().concat(properties.getEndpoint()))
                .queryParam(KEY_QUERY_PARAM, properties.getKey())
                .queryParam(FORMAT_QUERY_PARAM, properties.getFormat())
                .queryParam(DAYS_QUERY_PARAM, properties.getDays())
                .queryParam(TP_QUERY_PARAM, properties.getTime())
                .queryParam(MCA_QUERY_PARAM, properties.getMca());
    }
}
