package ro.fitbit.oana.service.weather.provider;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import ro.fitbit.oana.service.weather.data.Weather;
import ro.fitbit.oana.service.weather.data.WeatherRecord;
import ro.fitbit.oana.service.weather.exceptions.CityNotFoundException;
import ro.fitbit.oana.service.weather.exceptions.WeatherProviderAPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * World Weather Online API provider.
 */
@Slf4j
@Component
public class WorldWeatherProvider implements WeatherProvider {

    final RestTemplate restTemplate;

    final UriComponentsBuilder weatherURIBuilder;

    private static final String Q_QUERY_PARAM = "q";

    @Autowired
    public WorldWeatherProvider(RestTemplate restTemplate, UriComponentsBuilder weatherURIBuilder) {
        this.restTemplate = Objects.requireNonNull(restTemplate, "restTemplate");
        this.weatherURIBuilder = Objects.requireNonNull(weatherURIBuilder, "weatherURIBuilder");
    }

    @Override
    public WeatherRecord getWeatherData(String city) throws CityNotFoundException, WeatherProviderAPIException {
        try {
            UriComponents uriComponents = weatherURIBuilder
                    .replaceQueryParam(Q_QUERY_PARAM, city)
                    .build();
            log.info("Send request to World Weather Online API for city {} ", city);
            ResponseEntity<String> response = restTemplate.getForEntity(uriComponents.toUri(), String.class);

            if (response != null && response.getStatusCode().equals(HttpStatus.OK)) {
                return parseCity(response.getBody());
            }
        } catch (ClassCastException e) {
            throw new CityNotFoundException("Weather for this city was not found");
        } catch (IOException e) {
            log.error("Failed to get weather data from World Weather Online API due to ", e);
        }
        throw new WeatherProviderAPIException("Failed to get weather data from Weather API");
    }

    /**
     * Parse a WeatherRecord object with its weather data from JSON data received from Weather API.
     *
     * @param json String JSON data received from Weather API.
     * @return WeatherRecord weather with current and forecast data.
     * @throws IOException if something wrong happens during serialization.
     */
    private WeatherRecord parseCity(String json) throws IOException {
        List<Weather> forecastWeather = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);

        JsonNode forecastNode = root.at("/data/weather");
        ArrayNode forecast = (ArrayNode) forecastNode;
        int i = 0;
        for (JsonNode node : forecast) {
            if (i != 0) {
                forecastWeather.add(parseForecast(node.at("/hourly/0")));
            }
            i += 1;
        }
        return new WeatherRecord(parseCurrent(root.at("/data/current_condition/0")), forecastWeather);
    }

    /**
     * Parse forecast weather object from hourly array entry node.
     *
     * @param node JSON node with data to be serialized.
     * @return Weather object with forecast data.
     */

    private Weather parseForecast(JsonNode node) {
        return Weather.builder()
                .tempC(node.at("/tempC").asInt())
                .tempF(node.at("/tempF").asInt())
                .humidity(node.at("/humidity").asDouble() / 100)
                .condition(node.at("/weatherDesc/0/value").asText())
                .build();
    }

    /**
     * Parse current weather object from current condition JSON node.
     *
     * @param node JSON node with data to be serialized.
     * @return Weather object with current data.
     */
    private Weather parseCurrent(JsonNode node) {
        return Weather.builder()
                .tempC(node.at("/temp_C").asInt())
                .tempF(node.at("/temp_F").asInt())
                .humidity(node.at("/humidity").asDouble() / 100)
                .condition(node.at("/weatherDesc/0/value").asText())
                .build();
    }
}
