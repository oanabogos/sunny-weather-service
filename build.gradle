buildscript {

    project.ext {
        jdkVersion = '1.8'
        springBootVersion = '1.4.3.RELEASE'
        jacksonVersion = '2.8.6'
        apacheCommonsVersion = '4.1'
        commonsLangVersion = '3.4'
        lombokVersion = '1.16.18'
        logbackVersion = '1.1.8'
        janinoVersion = '3.0.6'
        guavaVersion = '11.0.2'
        slf4jVersion = '1.7.22'
        findbugsVersion = '3.0.1'
        junitVersion = '4.12'
    }

    dependencies {
        classpath("io.spring.gradle:dependency-management-plugin:0.6.0.RELEASE")
        classpath("org.springframework.boot:spring-boot-gradle-plugin:1.4.3.RELEASE")
        classpath("org.projectlombok:lombok:1.16.12")
    }

    configurations {
        compile.exclude module: 'tomcat-embed-el'
        compile.exclude module: 'spring-boot-starter-tomcat'
    }
}

plugins {
    id 'java'
    id 'idea'
    id 'findbugs'
    id 'checkstyle'
    id 'org.springframework.boot' version '1.4.3.RELEASE'
}

findbugs {
    toolVersion = '3.0.1'
    effort = 'max'
    reportLevel = 'low'
    reportsDir = file('build/reports/quality/findbugsReports')
    excludeFilter = file('quality/config/findbugs_exclude.xml')
}

tasks.withType(FindBugs) {
    reports {
        xml.enabled false
        html.enabled true
    }
}

checkstyle {
    toolVersion = '7.4'
    configFile = file('quality/config/checkstyle.xml')
    reportsDir = file('build/reports/quality/checkstyleReports')
}

tasks.withType(Checkstyle) {
    reports {
        xml.enabled false
        html.enabled true
    }
}

task wrapper(type: Wrapper) {
    gradleVersion = '4.2' //version required
}

task stage(dependsOn: ['build', 'clean'])
build.mustRunAfter clean

task copyToLib(type: Copy) {
    into "$buildDir/lib"
    from(configurations.compile)
}

stage.dependsOn(copyToLib)

group 'ro.fitbit.oana.service.weather'
version '1.0'

apply plugin: 'java'

sourceCompatibility = 1.8


repositories {
    mavenCentral()
}

dependencies {
    compileOnly group: 'org.projectlombok', name: 'lombok', version: lombokVersion
    compile group: 'ch.qos.logback', name: 'logback-classic', version: logbackVersion
    compile group: 'ch.qos.logback', name: 'logback-core', version: logbackVersion
    compile group: 'org.codehaus.janino', name: 'janino', version: janinoVersion
    compile group: 'org.slf4j', name: 'slf4j-api', version: slf4jVersion
    compile group: 'org.apache.commons', name: 'commons-lang3', version: commonsLangVersion

    compile group: 'org.springframework.boot', name: 'spring-boot-starter-web', version: springBootVersion
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-jetty', version: springBootVersion
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-actuator', version: springBootVersion
    compile group: 'org.springframework.boot', name: 'spring-boot-configuration-processor', version: springBootVersion

    compile group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: jacksonVersion
    compile group: 'org.apache.commons', name: 'commons-collections4', version: apacheCommonsVersion
    compile group: 'com.google.guava', name: 'guava', version: guavaVersion


    compile group: 'com.google.code.findbugs', name: 'annotations', version: findbugsVersion
    compile group: 'com.google.code.findbugs', name: 'jsr305', version: findbugsVersion

    testCompile group: 'org.springframework.boot', name: 'spring-boot-starter-test', version: springBootVersion
    testCompile group: 'junit', name: 'junit', version: junitVersion
}

compileJava.dependsOn(processResources)